<?php

namespace HotWire\Routing;

class Route
{

    /**
     * pattern
     * @var string
     */
    private $pattern;

    /**
     * default values when using placeholder
     * @var array
     */
    private $defaults=array();

    // TODO: need to handle http post, get, ...
    // TODO: need to regex to validate params
    public function __construct($pattern, array $defaults = array())
    {
        $this->pattern=$pattern;
        $this->defaults=$defaults;
    }

    /**
     * get route pattern
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * set route pattern
     * @param string $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern=$pattern;

        return $this;
    }

    /**
     * set default values
     * @return array
     */
    public function getDefaults()
    {
        return $this->defaults;
    }

    /**
     * set default values
     * @param array $defaults
     */
    public function setDefaults(array $defaults)
    {
        $this->defaults=$defaults;

        return $this;
    }
}
