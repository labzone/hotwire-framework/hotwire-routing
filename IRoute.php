<?php

namespace HotWire\Routing;

interface IRoute
{
    /**
     * get routes
     * @return array
     */
    public static function getRoutes();
}
