<?php

namespace HotWire\Routing;

/**
 * holds collection of routes
 */
class RouteCollection
{
    /**
     * routes object
     * @var array
     */
    private static $routes=array();

    /**
     * defaults values
     * @var array
     */
    private static $defaults=array();

    /**
     * set routes
     * @param array $routes
     */
    public static function setRoutes(array $routes)
    {
        self::$routes=$routes;
    }

    /**
     * get routes
     * @return [type] [description]
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * add route to collection
     * @param string $name  unique name
     * @param Route  $route
     */
    public static function add($name,Route $route)
    {
        self::$routes[$name]=$route;
    }
}
