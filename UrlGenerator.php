<?php

namespace HotWire\Routing;

/**
 * generate URL
 */
class UrlGenerator
{
    /**
     * generate URL
     * @param  string $routeName  route name
     * @param  array  $parameters route parameters
     * @return string URL
     */
    public static function generate($routeName, array $parameters = array())
    {
        $route=RouteCollection::$routes[$routeName];
        $url=$route->pattern;
        if (!$parameters) {
            return self::parameterReplace($route->pattern, $route->defaults);
        }

        return self::parameterReplace($route->pattern, $parameters);
    }

    /**
     * set parameters in route
     * @param  string $route      route pattern
     * @param  array  $parameters route parameters
     * @return string URL
     */
    public static function parameterReplace($route, array $parameters = array())
    {
        foreach ($parameters as $key => $parameter) {
            $route=str_replace("{{$key}}", $parameter, $route);
        }

        return $route;
    }
}
