<?php

namespace HotWire\Routing;

use HotWire\Routing\Exception\ResourceNotFoundException;

/**
 * URL Matcher
 */
class Matcher
{
    /**
     * match URL to a defined route
     * @param  string                    $path
     * @return array                     route name, parameters, controller action
     * @throws ResourceNotFoundException
     */
    public function match($path)
    {
        foreach (RouteCollection::getRoutes() as $name => $route) {
            if ($route == $path) {
                return true;
            }
            $pattern=$route->getPattern();
            $defaults=$route->getDefaults();
            foreach ($defaults as $key => $default) {
                $pattern=str_replace("{{$key}}","(.*?)", $pattern);
            }
            $pattern=str_replace('/','\/', $pattern);
            $match=null;
            if (preg_match("/^{$pattern}$/",$path, $match)) {
                $controller=$defaults['_controller'];
                array_shift($match);
                unset($defaults['_controller']);

                return array(
                    'route'=>$name,
                    'parameters'=>array_combine(array_keys($defaults),$match),
                    'controller'=>"{$controller}Action"
                );
            }
        }

        throw new ResourceNotFoundException();
    }
}
