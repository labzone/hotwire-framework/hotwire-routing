<?php

namespace HotWire\Routing\Exception;

/**
 * 404 exception
 */
class ResourceNotFoundException extends \Exception
{
    /**
     * override code variable
     * @var integer
     */
    protected $code=404;

    /**
     * override message
     * @var string
     */
    protected $message='Page not found';
}
